<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A90436">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The way to peace and happiness whereunto are annexed some useful sayings in verse and prose.</title>
    <author>Pennyman, John, 1628-1706.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A90436 of text R42610 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing P1425). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A90436</idno>
    <idno type="STC">Wing P1425</idno>
    <idno type="STC">ESTC R42610</idno>
    <idno type="EEBO-CITATION">36282450</idno>
    <idno type="OCLC">ocm 36282450</idno>
    <idno type="VID">150160</idno>
    <idno type="PROQUESTGOID">2240889576</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A90436)</note>
    <note>Transcribed from: (Early English Books Online ; image set 150160)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2234:10)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The way to peace and happiness whereunto are annexed some useful sayings in verse and prose.</title>
      <author>Pennyman, John, 1628-1706.</author>
     </titleStmt>
     <extent>1+ p.</extent>
     <publicationStmt>
      <publisher>Printed for the author,</publisher>
      <pubPlace>London :</pubPlace>
      <date>[1681?]</date>
     </publicationStmt>
     <notesStmt>
      <note>Incomplete: title page only.</note>
      <note>Attributed to John Pennyman by Wing (2nd ed.).</note>
      <note>Date of publication from Wing (2nd ed.).</note>
      <note>Reproduction of original in the Friends' Library (London, England)</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Christian life -- Early works to 1800.</term>
     <term>Peace.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The way to peace and happiness: whereunto are annexed some useful sayings in verse and prose,</ep:title>
    <ep:author>Pennyman, John, </ep:author>
    <ep:publicationYear>1681</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>63</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-07</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-07</date>
    <label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-08</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-08</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A90436-t">
  <body xml:id="A90436-e0">
   <div type="title_page" xml:id="A90436-e10">
    <pb facs="tcp:150160:1" rend="simple:additions" xml:id="A90436-001-a"/>
    <p xml:id="A90436-e20">
     <w lemma="the" pos="d" xml:id="A90436-001-a-0010">THE</w>
     <w lemma="way" pos="n1" xml:id="A90436-001-a-0020">WAY</w>
     <w lemma="to" pos="acp" xml:id="A90436-001-a-0030">TO</w>
     <w lemma="peace" pos="n1" xml:id="A90436-001-a-0040">PEACE</w>
     <w lemma="and" pos="cc" xml:id="A90436-001-a-0050">and</w>
     <w lemma="happiness" pos="n1" xml:id="A90436-001-a-0060">HAPPINESS</w>
     <pc xml:id="A90436-001-a-0070">:</pc>
     <w lemma="whereunto" pos="crq" xml:id="A90436-001-a-0080">Whereunto</w>
     <w lemma="be" pos="vvb" xml:id="A90436-001-a-0090">are</w>
     <w lemma="annex" pos="vvn" xml:id="A90436-001-a-0100">annexed</w>
     <w lemma="some" pos="d" xml:id="A90436-001-a-0110">some</w>
     <w lemma="useful" pos="j" xml:id="A90436-001-a-0120">Useful</w>
     <w lemma="say" pos="n2-vg" xml:id="A90436-001-a-0130">SAYINGS</w>
     <pc xml:id="A90436-001-a-0140">,</pc>
     <w lemma="in" pos="acp" xml:id="A90436-001-a-0150">IN</w>
     <w lemma="verse" pos="n1" xml:id="A90436-001-a-0160">VERSE</w>
     <w lemma="and" pos="cc" xml:id="A90436-001-a-0170">and</w>
     <w lemma="prose" pos="n1" xml:id="A90436-001-a-0180">PROSE</w>
     <pc xml:id="A90436-001-a-0190">,</pc>
    </p>
    <p xml:id="A90436-e30">
     <q xml:id="A90436-e40">
      <w lemma="you" pos="pn" xml:id="A90436-001-a-0200">Ye</w>
      <w lemma="shall" pos="vmb" xml:id="A90436-001-a-0210">shall</w>
      <w lemma="teach" pos="vvi" xml:id="A90436-001-a-0220">Teach</w>
      <w lemma="they" pos="pno" xml:id="A90436-001-a-0230">them</w>
      <w lemma="your" pos="po" xml:id="A90436-001-a-0240">your</w>
      <w lemma="child" pos="n2" xml:id="A90436-001-a-0250">Children</w>
      <pc xml:id="A90436-001-a-0260">,</pc>
      <w lemma="speak" pos="vvg" xml:id="A90436-001-a-0270">speaking</w>
      <w lemma="of" pos="acp" xml:id="A90436-001-a-0280">of</w>
      <w lemma="they" pos="pno" xml:id="A90436-001-a-0290">them</w>
      <w lemma="when" pos="crq" xml:id="A90436-001-a-0300">when</w>
      <w lemma="thou" pos="pns" xml:id="A90436-001-a-0310">thou</w>
      <w lemma="sit" pos="vv2" xml:id="A90436-001-a-0320">sittest</w>
      <w lemma="in" pos="acp" xml:id="A90436-001-a-0330">in</w>
      <w lemma="thy" pos="po" xml:id="A90436-001-a-0340">thine</w>
      <hi xml:id="A90436-e50">
       <w lemma="house" pos="n1" xml:id="A90436-001-a-0350">House</w>
       <pc xml:id="A90436-001-a-0360">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A90436-001-a-0370">and</w>
      <w lemma="when" pos="crq" xml:id="A90436-001-a-0380">when</w>
      <w lemma="thou" pos="pns" xml:id="A90436-001-a-0390">thou</w>
      <w lemma="walk" pos="vv2" xml:id="A90436-001-a-0400">walkest</w>
      <w lemma="by" pos="acp" xml:id="A90436-001-a-0410">by</w>
      <w lemma="the" pos="d" xml:id="A90436-001-a-0420">the</w>
      <hi xml:id="A90436-e60">
       <w lemma="way" pos="n1" xml:id="A90436-001-a-0430">Way</w>
       <pc xml:id="A90436-001-a-0440">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A90436-001-a-0450">and</w>
      <w lemma="when" pos="crq" xml:id="A90436-001-a-0460">when</w>
      <w lemma="thou" pos="pns" xml:id="A90436-001-a-0470">thou</w>
      <w lemma="lie" pos="vv2" xml:id="A90436-001-a-0480">liest</w>
      <hi xml:id="A90436-e70">
       <w lemma="down" pos="acp" xml:id="A90436-001-a-0490">Down</w>
       <pc xml:id="A90436-001-a-0500">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A90436-001-a-0510">and</w>
      <w lemma="when" pos="crq" xml:id="A90436-001-a-0520">when</w>
      <w lemma="thou" pos="pns" xml:id="A90436-001-a-0530">thou</w>
      <w lemma="rise" pos="vv2" xml:id="A90436-001-a-0540">risest</w>
      <hi xml:id="A90436-e80">
       <w lemma="Up." pos="nn1" xml:id="A90436-001-a-0550">Up.</w>
       <pc unit="sentence" xml:id="A90436-001-a-0560"/>
      </hi>
     </q>
    </p>
    <p xml:id="A90436-e90">
     <hi xml:id="A90436-e100">
      <w lemma="London" pos="nn1" xml:id="A90436-001-a-0570">London</w>
      <pc xml:id="A90436-001-a-0580">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A90436-001-a-0590">Printed</w>
     <w lemma="for" pos="acp" xml:id="A90436-001-a-0600">for</w>
     <w lemma="the" pos="d" xml:id="A90436-001-a-0610">the</w>
     <w lemma="author" pos="n1" xml:id="A90436-001-a-0620">Author</w>
     <pc unit="sentence" xml:id="A90436-001-a-0630">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A90436-t-b"/>
 </text>
</TEI>
